## 0.1.1 / 2024-06-26
* Avoid `unwrap()` and document remaining usage

## 0.1.0 / 2024-04-26
* Initial commit
