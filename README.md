deb-version(7)
==============

This is a Rust port of the version-related code in [`dpkg`](https://en.wikipedia.org/wiki/Dpkg), the
base layer of package management in Debian. The goal is to match the behavior of dpkg as closely as
possible, while providing convenient Rust abstractions. The specification can be found in the
[`deb-version(7)` man page](https://dyn.manpages.debian.org/jump?q=deb-version).

## Functionality

You can parse and validate versions, as well as check equality and sort
in the same way dpkg would.

```rust
use deb_version7::DebVersion;
use std::str::FromStr;

let first = DebVersion::from_str("1.0.0-1").unwrap();
let second = DebVersion::from_str("1.0.0-1~bpo12+1").unwrap();
assert!(first > second);
```

While the `DebVersion` is created using the Rust `String`/`&str` type, deb versions are ASCII-only. Any non-ASCII characters
will be immediately rejected.

## Implementation

The port was done using version [f268c3733](https://git.dpkg.org/git/dpkg/dpkg.git/tree/?id=f268c37333f6569f2a1d280446cc48ece1e41199)
of the [dpkg Git repository](https://git.dpkg.org/git/dpkg/dpkg.git/).

The rough structure of the upstream C code was retained to ease future updates.
No effort has been made into evaluating performance (yet).

## Credits

`deb-version7` is available under the GPL v3, or at your option, any later version. The
primary copyright holders are Ian Jackson and Guillem Jover, who wrote the original C code.
Kunal Mehta (with the assistance of ChatGPT 3.5) did the Rust port.

Each source file contains where the code originally came from in the dpkg codebase retains
the required license headers (with minimal syntax changes).
