// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: Copyright © 2024 Kunal Mehta <legoktm@debian.org>

use thiserror::Error as ThisError;

#[derive(ThisError, Debug, PartialEq, Eq)]
pub enum Error {
    #[error("Only ASCII characters are allowed in versions")]
    NotAscii,
    #[error("version string is empty")]
    EmptyVersion,
    #[error("version string has embedded spaces")]
    EmbeddedSpaces,
    #[error("epoch in version is not valid: {0}")]
    InvalidEpoch(String),
    #[error("nothing after colon in version number")]
    NothingAfterColon,
    #[error("revision number is empty")]
    EmptyRevision,
    #[error("version number is empty")]
    EmptyVersionNumber,
    #[error("version number does not start with digit")]
    NonDigitVersion,
    #[error("invalid character in version number")]
    InvalidCharacterInVersion,
    #[error("invalid character in revision number")]
    InvalidCharacterInRevision,
}
