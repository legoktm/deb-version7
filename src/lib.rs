#![deny(clippy::all)]
#![doc = include_str!("../README.md")]
// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: Copyright © 2024 Kunal Mehta <legoktm@debian.org>

mod error;
mod parsehelp;
#[cfg(test)]
mod t_version;
mod version;

pub use crate::error::Error;
pub use crate::version::DebVersion;
